import logo from "./logo.svg";
import "./App.css";
import ReduxMini from "./ReduxMini/ReduxMini";

function App() {
  return (
    <div className="App">
      <ReduxMini />
    </div>
  );
}

export default App;
