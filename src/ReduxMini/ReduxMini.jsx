import React, { Component } from "react";
import { connect } from "react-redux";

class ReduxMini extends Component {
  render() {
    return (
      <div className="pt-5">
        <button
          onClick={() => {
            this.props.handleChangeQuantity("Alice", -1);
          }}
          className="btn btn-warning"
        >
          -
        </button>
        <strong className="mx-3">{this.props.soLuong}</strong>
        <button
          onClick={() => {
            this.props.handleChangeQuantity("Bob", 1);
          }}
          className="btn btn-danger"
        >
          +
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    soLuong: state.amount.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeQuantity: (id, value) => {
      let action = {
        type: "THAY_DOI_SO_LUONG",
        payload: {
          id,
          value,
        },
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReduxMini);
