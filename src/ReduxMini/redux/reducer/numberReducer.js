let initialState = {
  number: 1,
};

export const numberReducer = (state = initialState, action) => {
  switch (action.type) {
    case "THAY_DOI_SO_LUONG": {
      let { id, value } = action.payload;
      console.log("id: ", id);
      return { ...state, number: (state.number += value) };
    }
    default: {
      return state;
    }
  }
};
